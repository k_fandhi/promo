package com.metraplasa.training.microservices.promo.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.metraplasa.training.microservices.promo.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String>{

}
